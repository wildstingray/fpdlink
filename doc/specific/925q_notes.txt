Package:

Desc: Texas Instruments RHS 48
Min Body Span 1: 6.85
Max Body Span 1: 7.15
Min Body Span 2: 6.85
Max Body Span 2: 7.15
Min Lead Width: 0.18
Max Lead Width: 0.3
Min Lead Length: 0.3
Max Lead Length: 0.5
Min Height: 0.7
Max Height: 0.8
Min Standoff Height: 0
Max Standoff Height: 0.05
Min Lead Height: 0.1
Max Lead Height: 0.2
Number Of Pins 1: 12
Number of Pins 2: 12
Pitch 1: 0.5
Pitch 2: 0.5
Pin 1 Location: corner of package
Min Thermal Pad Size 1: 5.0
Max Thermal Pad Size 1: 5.2
Min Thermal Pad Size 2: 5.0
Max Thermal Pad Size 2: 5.2
Board Density Level: Nominal
Drawing Note: SQA48A